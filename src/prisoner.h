#pragma once

#include <stddef.h>

typedef enum {COOPERATE, DEFECT} decision_t;
typedef decision_t (*strategy_t)(decision_t const* history, size_t turn);
typedef struct {void* handle; char* name;} prisoner_t;

prisoner_t* prisoner_intake(size_t* o_len);
void        prisoner_parole(prisoner_t*, size_t len);

typedef struct {size_t A, B;} tally_t; 
tally_t game(prisoner_t A, prisoner_t B, size_t rounds);