#include <prisoner.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main () {
	size_t count;
	prisoner_t* prisoners = prisoner_intake(&count);

	unsigned seed = 0;
	getentropy(&seed, sizeof(seed));
	srand(seed);

	if (count == 0 || prisoners == NULL)
		return -1;

	for (size_t i = 0; i < count; i++)
	for (size_t j = 0; j < count; j++) {
		size_t rounds = 200 + rand() % 200;
		tally_t tally = game(prisoners[i], prisoners[j], rounds);
		
		float ana = (float) tally.A / rounds;
		float bob = (float) tally.B / rounds;

		printf("%-16s v. %-16s : %.3f %.3f\n", 
			prisoners[i].name, prisoners[j].name,
			ana,               bob 
		);
	}

	return 0;
}