#include <prisoner.h>

#include <errno.h>
#include <dlfcn.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>

#define λ(Ret, Args, Body) ({Ret λ Args Body λ;})

static void load_cb(void(*cb)(prisoner_t)) {
	char const* path = "cells";
	DIR* cells = opendir(path);
	struct dirent *dp;
	while ((dp = readdir(cells)) != NULL) {
		char fname[strlen(path) + strlen(dp->d_name) + 2];
		memset(fname, 0, sizeof(fname));
		char *end = fname;
		end = strcat(end, path);
		end = strcat(end, "/");
		end = strcat(end, dp->d_name);

		struct stat statbuf;
		if (stat(fname, &statbuf) == -1)
			continue;
		if ((statbuf.st_mode & S_IFMT) == S_IFDIR)
			continue;

		char const* ext = memrchr(fname, '.', sizeof(fname));
		if (ext == NULL)
			continue;
		if (strcmp(ext, ".so"))
			continue;
		

		void* handle = dlopen(fname, RTLD_NOW | RTLD_DEEPBIND);
		if (! handle )
			continue;
		if (! dlsym(handle, "strategy")) {
			dlclose(handle);
			continue;
		}

		cb((prisoner_t){.handle = handle, .name = dp->d_name});
	}

	closedir(cells);
}

prisoner_t* prisoner_intake(size_t* o_len) {
	*o_len = 0;
	load_cb(λ(void, (prisoner_t p), {
		*o_len += 1;
		dlclose(p.handle);
	}));

	if (*o_len == 0)
		return NULL;

	prisoner_t* prisoners = calloc(*o_len, sizeof(prisoner_t));
	if (prisoners == NULL)
		return NULL;

	size_t i = 0;
	load_cb(λ(void, (prisoner_t p), {
		if (i < *o_len) {
			prisoners[i++] = p;
		} else {
			dlclose(p.handle);
			free(p.name);
		}
	}));

	return prisoners;
}

void prisoner_parole(prisoner_t* prisoners, size_t len) {
	if (prisoners == NULL || len == 0)
		return;
	for (size_t i = 0; i < len; i++) {
		dlclose(prisoners[i].handle);
		free(prisoners[i].name);
	}
	free(prisoners);
}

tally_t game(prisoner_t A, prisoner_t B, size_t rounds) {
	decision_t A_moves[rounds];
	decision_t B_moves[rounds];
	strategy_t alice = dlsym(A.handle, "strategy");
	strategy_t bob   = dlsym(B.handle, "strategy");

	tally_t tally = {0};

	for (size_t i = 0; i < rounds; i++) {
		decision_t A_move = alice(B_moves, i);
		decision_t B_move =   bob(A_moves, i);
		A_moves[i] = A_move;
		B_moves[i] = B_move;

		if (A_move == COOPERATE && B_move == COOPERATE) {
			tally.A += 3;
			tally.B += 3;
		} else
		if (A_move == COOPERATE && B_move == DEFECT) {
			tally.A += 0;
			tally.B += 5;
		} else 
		if (A_move == DEFECT    && B_move == COOPERATE) {
			tally.A += 5;
			tally.B += 0;
		} else 
		if (A_move == DEFECT    && B_move == DEFECT) {
			tally.A += 1;
			tally.B += 1;
		}
	}

	return tally;
}