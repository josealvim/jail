.PHONY: clean 

DEPENDENCIES = 

CC          ?= gcc
CC_VERSION  ?= c2x

CC_DYNAMIC  := $(shell find cells -name "*.c")
CC_SHARED   := $(patsubst %.c, %.so,  ${CC_DYNAMIC})

CC_STATIC   := $(shell find src   -name "*.c")
CC_OBJECTS  := $(patsubst %.c, %.o,   ${CC_STATIC})

CC_DEPENDS  := $(patsubst %.c, %.dep, ${CC_STATIC} ${CC_DYNAMIC})

BUILD_FLAGS += -g -O0 -D_POSIX_C_SOURCE=202401L -D_GNU_SOURCE

DEPS_CFLAGS := $(shell pkg-config --cflags ${DEPENDENCIES} 2>/dev/null)
DEPS_LIBS   := $(shell pkg-config --libs   ${DEPENDENCIES} 2>/dev/null)
DEPS_LIBS   += -lm -lpthread -lc

CC_INVOKE   := ${CC} -std=${CC_VERSION} ${DEPS_CFLAGS} ${BUILD_FLAGS} -Isrc -Wall -Wextra -Werror

all: jail.out inmates
	@echo done!

jail.out: ${CC_OBJECTS} 
	${CC_INVOKE} -o $@ ${CC_OBJECTS} ${DEPS_LIBS}

inmates: ${CC_SHARED} 

clean-deps: 
	find -type f -name "*.dep" -exec rm {} \;

clean: 
	rm -f jail.out
	find -type f -name "*.o"   -exec rm {} \;
	find -type f -name "*.dep" -exec rm {} \;

%.dep: %.c
	${CC_INVOKE} -MM $< >> "$@"

%.o: %.c %.dep
	${CC_INVOKE} -c -o $@ $<

%.so: %.c %.dep
	${CC_INVOKE} -fPIC -shared -o $@ $<

include ${CC_DEPENDS}