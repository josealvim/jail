#include <prisoner.h>

decision_t strategy (decision_t const* history, size_t turn) {
	if (turn == 0)
		return COOPERATE;
	else
		return history[turn - 1];
}