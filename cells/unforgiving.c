#include <prisoner.h>

decision_t strategy (decision_t const* history, size_t turn) {
	decision_t decision = COOPERATE;
	for (size_t i = 0; i < turn; i++) 
	if (history[i] == DEFECT) {
		decision = DEFECT; 
		break;
	}
	return decision;
}