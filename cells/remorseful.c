#include <prisoner.h>
#include <stdlib.h>

decision_t strategy (decision_t const* history, size_t turn) {
	if (turn == 0)
		return COOPERATE;
	if (turn < 20)
		return history[turn-1];

	size_t feels_bad = 0;
	for (size_t i = 1; i < 20; i++)
		feels_bad += history[turn-i] == COOPERATE;
	if (rand() % 100 + feels_bad/2 < 10)
		return DEFECT;
	else
		return history[turn-1];
}