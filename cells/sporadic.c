#include <prisoner.h>
#include <stdlib.h>

decision_t strategy (decision_t const* history, size_t turn) {
	if (turn == 0)
		return COOPERATE;
	if (rand() % 100 < 10)
		return DEFECT;
	else
		return history[turn-1];
}