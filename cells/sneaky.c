#include <prisoner.h>

decision_t strategy (decision_t const* history, size_t turn) {
	if (turn == 0)
		return COOPERATE;
	if (turn < 20)
		return history[turn-1];
	size_t hits = 0;
	for (size_t i = 1; i < 20; i++)
		hits += history[turn-i] == DEFECT;
	if (hits == 0)
		return DEFECT;
	return history[turn-1];
}